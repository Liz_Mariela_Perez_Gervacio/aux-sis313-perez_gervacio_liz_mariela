
# **_PRACTICA Nro7_**

### **1: Realice un video explicando lo siguiente:**
**a.** La creación de una carpeta en el disco local C:, mediante comandos desde la
terminal

**b.** La forma de cambiar de directorios utilizando comandos

https://github.com/lizmariela/imagenes/assets/147956090/a005ae4b-54fe-47e7-961e-e501b42e65d1

### **2: Adjunte capturas de pantalla del mismo proceso, tres veces más.**
![Captura de pantalla 2024-04-23 192623](https://github.com/lizmariela/imagenes/assets/147956090/219815bb-ddac-490e-96b4-9df4350c4954)

![Captura de pantalla 2024-04-23 192711](https://github.com/lizmariela/imagenes/assets/147956090/7f79fe3a-4c46-4be3-ade4-94ce5c51a5bd)
### **3: Que comandos se utilizó en las anteriores preguntas.**
Se uso los comandos **"cd..","md","cd"**.
### **4 : Explique 5 comandos para ser utilizados en la terminal y su forma de usarse en Windows (Terminal o Powershell) y en Linux (bash)**.

### 1. Navegación de directorios:

**Comando: cd**

**Función:** Navegar entre directorios del sistema.

**Uso en Windows:** cd ruta_directorio

**Uso en Linux:** cd ruta_directorio
### 2. Listado de contenido:

**Comando: ls**

**Función:** Mostrar el contenido del 
directorio actual.

**Uso en Windows:** ls

**Uso en Linux:** ls
### 3. Visualización de archivos:

**Comando: cat**

**Función:** Mostrar el contenido de un archivo de texto.

**Uso en Windows:** cat nombre_archivo

**Uso en Linux:** cat nombre_archivo

### 4. Copiado de archivos:

**Comando: cp**

**Función:** Copiar archivos o directorios.

**Uso en Windows:** cp archivo_origen archivo_destino

**Uso en Linux:** cp archivo_origen archivo_destino
 
### 5. Creación de directorios:

**Comando: mkdir**

**Función:** Crear directorios.

**Uso en Windows:** mkdir nombre_directorio

**Uso en Linux:** mkdir nombre_directorio
## 🚀 About Me
I'm a full stack developer...

