
# **_PRACTICA Nro8_**
#### ***Nombre:Perez Gervacio Liz Mariela***

#### ***C.I:10478253***
#### 1 : Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal
![Captura de pantalla 2024-04-23 204459](https://github.com/lizmariela/imagenes/assets/147956090/c6d9257d-c1c0-41ea-acbc-40b6aa9f8afb)
### 2: Realice un video explicando lo siguiente:
**a.** La creación de una carpeta en el disco local C:, mediante comandos desde la terminal

**b**. La creación de un proyecto utilizando React con Next.js

https://github.com/lizmariela/imagenes/assets/147956090/39967239-ab51-40f9-8914-57f809052207

### 3 :¿Qué comandos se utilizó en las anteriores preguntas?
Se usaron : **"node --version , npm --version , cd.."**
***
### 4:¿Qué errores tuvo al crear el proyecto? Tome en cuenta los errores que tuvo al crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE ABRIL DE 2024. IMPORTANTE: De preferencia, adjunte las capturas de dichos errores.
El error de no crear una carpeta "npm" antes de la creacion del proyecto 


## 🚀 About Me
I'm a full stack developer...

