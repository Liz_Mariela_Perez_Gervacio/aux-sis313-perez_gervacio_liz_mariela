type PiecabProps={
    text:string,
    tit:any,
}
 const Piecab =(props:PiecabProps)=>{
    const{text,tit}=props;
    return(
        <div className="pie">
            <h4 className="a">{text}</h4>
            <h3 className="e">{tit}</h3>
            
        </div>
    )
}
export default Piecab