type TitleProps={
    text:string,
    par:string,
}
 const Title =(props:TitleProps)=>{
    const{text,par}=props;
    return(
        <div className="titulo1">
            <h1>{text}</h1>
            <p>{par}</p>
        </div>
    )
}
export default Title