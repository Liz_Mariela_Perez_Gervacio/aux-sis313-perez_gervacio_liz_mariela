import { text } from "stream/consumers";

export default function Header(props:{imagen:any,text:any}){
    return(
        <div className="logo">
          <img src={props.imagen} alt="" />
         <h2>{props.text}</h2> 
        </div>
    )
}