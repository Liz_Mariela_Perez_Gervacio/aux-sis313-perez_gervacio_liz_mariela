type IconosProps={
    icono:string,
}
 const Iconos =(props:IconosProps)=>{
    const{icono}=props;
    return(
        <div className="icono">
            <button><img src={icono} alt="" /></button>
        </div>
    )
}
export default Iconos