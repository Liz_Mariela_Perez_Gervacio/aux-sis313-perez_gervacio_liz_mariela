import Image from "next/image";
import styles from "./page.module.css";
import Header from "./componentes/Header";
import Title from "./componentes/Title";
import Cajas from "./componentes/Cajas";
import Piecab from "./componentes/Piecab";
import Iconos from "./componentes/Iconos";

export default function Home() {
  return (
    <main className={styles.main}>
      <Header imagen="/imagen/corvata.svg" text="Your Logo"></Header>
      <section >
        <div className="hero">
          <div className="pag1">
            <Title text="Login" par="Login to access your travelwise account"></Title>
            <Cajas text="Email" text2="Password" text3="Login"></Cajas>
            <Piecab text="Don’t have an account?" tit="Sign up"></Piecab>
            <div className="barra">
              <img src="/imagen/barra.svg" width="495px" height="550px" />
            </div>
            <div className="icono">
              <Iconos icono="/imagen/facebook.svg" ></Iconos>
              <Iconos icono="/imagen/google.svg" ></Iconos>
              <Iconos icono="/imagen/apple.svg"></Iconos>
            </div>
          </div>
          <div className="pag2">
            <img src="/imagen/mano.svg" width="550px" height="550px" ></img>
          </div>
        </div>
      </section>
    </main>
  );
}
