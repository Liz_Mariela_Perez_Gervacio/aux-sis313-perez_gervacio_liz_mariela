#   **Practica N°10**
### **Nombre**: Perez Gervacio Liz Mariela 

### **1: Link del template.**
- https://www.figma.com/file/BFmRicRpChwVBOPdA0V9VA/Header-for-a-website-about-a-hotel-for-cats-(Community)?type=design&t=nBlFue81JN2XAE3u-6

* https://www.figma.com/community/file/1204113366416049050
### **1:Link del proyecto en Figma utilizando el template.**
* https://www.figma.com/file/BFmRicRpChwVBOPdA0V9VA/Header-for-a-website-about-a-hotel-for-cats-(Community)?type=design&t=84W2yZfPTwpJowgG-6
* https://www.figma.com/file/KKyWQNXZVj1kOsDH4tNKlW/Online-Education-Website-Free-Template-(Community)?type=design&node-id=0-1&mode=design&t=4J2gvL70r2hzKLx4-0
### **1:Link del repositorio creado.**
https://gitlab.com/Liz_Mariela_Perez_Gervacio/react-with-liz_mariela_perez_gervacio.git
