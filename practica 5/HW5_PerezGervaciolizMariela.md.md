
`Liz Mariela Perez Gervacio`
# 🍽️ ***CSS Diner*** 🍽️
![Captura de pantalla 2024-03-31 032916_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/b5d2267f-73cb-4512-926b-3d2375efbd48)

* ## ***Level 1*** 🍽️
![imagen_2024-03-31_040818898_preview_rev_1 (1)](https://github.com/lizmariela/imagen/assets/147956090/ed2fcb46-9920-48e3-8f43-45bfee6b3779)
####    Se paso el nivel usando : ***plate***


* ## ***Level 2*** 🔴🍽️🔴
![imagen_2024-03-31_034846341_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/b2419f22-9267-46cc-b840-4c33e815b103)
#### Se paso el nivel usando : ***bento***
* ## ***Level 3*** 🍽️


![imagen_2024-03-31_035056736_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/9b2c7e62-b79e-443d-abb3-24b6e9a2e3de)
#### Se paso el nivel usando : ***#fancy***

* ## ***Level 4*** 🍽️ 🍎
![imagen_2024-03-31_035158771_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/1b21e3ce-66ff-48a8-81ea-bc9b9c510f8d)
#### Se paso el nivel usando : ***plate apple***

* ## ***Level 5*** 🍽️🥒
![imagen_2024-03-31_035309759_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/55873cc2-97e5-45c1-bdf4-d27d15957af4)
#### Se paso el nivel usando : ***#fancy pickle***

* ## ***Level 22*** 🍽️🥒🍎
![imagen_2024-03-31_035409464_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/76db2e32-2365-4cbb-9140-5b0dd7cecd71)
#### Se paso el nivel usando : ***:nth-of-type(2n+3)***

* ## ***Level 23*** 🍽️🥒🍅
![imagen_2024-03-31_035507652_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/8e88b0fb-28af-47e6-8135-77bf38cf4273)
#### Se paso el nivel usando : ***apple:only-of-type***

* ## ***Level 24*** 🥚🥚🥒🥒🍅🍅

![imagen_2024-03-31_035546510_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/24cb4930-f13c-49cd-ae96-384d487abea5)
#### Se paso el nivel usando : ***.small:last-of-type***

* ## ***Level 25*** 🟥🍽️🥒
![imagen_2024-03-31_035627710_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/14507b61-7914-4671-98f1-b86161438925)
##3# Se paso el nivel usando : ***bento:empty***

* ## ***Level 26*** 🍅🍎🍎🥚🥒
![imagen_2024-03-31_035823258_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/a0016675-56c5-40d1-a4bb-46a5189969cd)
#### Se paso el nivel usando : ***apple:not(.small)***

* ## ***Level 27*** 🍎🥒🥚
![imagen_2024-03-31_035913199_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/119dbe9b-3cc1-42f4-a4c0-d8fc0101086b)
#### Se paso el nivel usando : ***[for]***

* ## ***Level 28***🥒🍎🍽️🥚
![imagen_2024-03-31_040025954_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/d7ff00e6-47f1-492d-bd19-fdc65c030dea)
#### Se paso el nivel usando : ***plate[for]***

* ## ***Level 29***🍎🍎🥒🥚
![imagen_2024-03-31_040148168_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/9f8f4bf2-0ac1-4180-b90a-91afcc1f2ec9)
#### Se paso el nivel usando : ***[for="Vitaly"]***

* ## ***Level 30***🥒🍅🥚
![imagen_2024-03-31_040251809_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/b91b8cbb-2676-42cf-b55e-23cad72ecfd7)
#### Se paso el nivel usando : ***[FOR^="Sa"]***

* ## ***Level 31***🍅🥒🍎🥚🥒
![imagen_2024-03-31_040344049_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/1c45945d-6648-410e-b922-e4fb326ed000)
#### Se paso el nivel usando : ***[for$="ato"]***


* ## ***Level 32***🍅🥒🥚
![imagen_2024-03-31_040424083_preview_rev_1](https://github.com/lizmariela/imagen/assets/147956090/8ffc5d76-b0e5-4867-93ce-12c237253e66)
#### Se paso el nivel usando : **[for***="obb"]
 
 # ***FINAL***
![Captura de pantalla 2024-03-31 031623](https://github.com/lizmariela/imagen/assets/147956090/3ea6336d-e223-4389-87de-65ff49c24a2c)
![image](https://github.com/lizmariela/imagen/assets/147956090/b3339502-9ad5-4ce4-9080-a1e78678fd78)


