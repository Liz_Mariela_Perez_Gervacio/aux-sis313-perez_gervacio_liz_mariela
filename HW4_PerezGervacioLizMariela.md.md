
`Liz Mariela Perez Gervacio`
# ***GRID ATTACK***  🎮
![Captura de pantalla 2024-03-26 085619](https://github.com/lizmariela/imagen/assets/147956090/731bafb6-61f2-4ef4-9d51-107c9c071291)

## ***HARD*** 🗡️🩸
![Captura de pantalla 2024-03-26 090647](https://github.com/lizmariela/imagen/assets/147956090/4d661c11-1fa5-433c-9324-426cc2b344e3)

* ### ***LEVEL 10*** ⚔️ 
![Captura de pantalla 2024-03-26 013502](https://github.com/lizmariela/imagen/assets/147956090/e7be2777-3144-45ea-be7c-fd43b0c2a977)
 * ###  ***LEVEL 18*** ⚔️
![Captura de pantalla 2024-03-26 013816](https://github.com/lizmariela/imagen/assets/147956090/a8e270c1-b7e8-4e58-b5c6-e6722a826226)
 * ###  ***LEVEL 20*** ⚔️
![Captura de pantalla 2024-03-26 014036](https://github.com/lizmariela/imagen/assets/147956090/afd1e319-c9f0-4439-ae26-d64fa05b9fe6)
 * ###  ***LEVEL 29*** ⚔️
![Captura de pantalla 2024-03-26 014358](https://github.com/lizmariela/imagen/assets/147956090/38c806b1-58e2-45c5-abda-21589e10fdc1)
 * ###  ***LEVEL 30*** ⚔️
![Captura de pantalla 2024-03-26 014434](https://github.com/lizmariela/imagen/assets/147956090/a5822cd9-9b9c-4565-bf91-64e317efc320)
 * ###  ***LEVEL 40*** ⚔️
![Captura de pantalla 2024-03-26 014758](https://github.com/lizmariela/imagen/assets/147956090/3f9b8999-daaf-45f7-9d27-c606d6f09fa9)
 * ###  ***LEVEL 45*** ⚔️
![Captura de pantalla 2024-03-26 014921](https://github.com/lizmariela/imagen/assets/147956090/23cca78b-49a4-4a6e-beff-6f5c58330b28)
 * ###  ***LEVEL 50*** ⚔️
![Captura de pantalla 2024-03-26 015050](https://github.com/lizmariela/imagen/assets/147956090/6c0098da-70a4-4b3b-8ac5-f6eeb34b7494)
 * ###  ***LEVEL 60*** ⚔️
![Captura de pantalla 2024-03-26 015556](https://github.com/lizmariela/imagen/assets/147956090/3d006ced-8a53-4af2-9c6f-4818bdc05806)
 * ###  ***LEVEL 66*** ⚔️
![Captura de pantalla 2024-03-26 015845](https://github.com/lizmariela/imagen/assets/147956090/e1cb9483-569d-41bd-af1f-9d4e66eea479)
 * ###  ***LEVEL 70*** ⚔️
![Captura de pantalla 2024-03-26 020002](https://github.com/lizmariela/imagen/assets/147956090/93e68767-4306-4f06-9422-d2594f8bfc20)
 * ###  ***LEVEL 78*** ⚔️
![Captura de pantalla 2024-03-26 020209](https://github.com/lizmariela/imagen/assets/147956090/f885621d-8aea-4411-9060-680f6d30f868)

* ###  ***LEVEL 80*** ⚔️ 
![Captura de pantalla 2024-03-26 020846](https://github.com/lizmariela/imagen/assets/147956090/1144c5b6-5890-4aea-8185-cb2efd265248)
