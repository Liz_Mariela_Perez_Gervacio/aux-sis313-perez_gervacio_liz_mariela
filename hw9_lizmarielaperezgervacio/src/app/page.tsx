import Image from "next/image";
import Button from "@/componets/Button";
import Pokemon from "@/componets/Pokemon";
import Videos from "@/componets/Videos";
import Cuadrofinal from "@/componets/cuadrofinal";
export default function Home() {
  return (
    <main >
      <div className="letras">
      <Button title="HOLA"></Button>
      <Button title="ADIOS" transparente></Button>
      </div>

      <div className="puchamon">
        <Pokemon></Pokemon>
      </div>
      <div className="uuu">
       <Videos title ="Videito" parafo="internet" video="https://github.com/lizmariela/imagen/assets/147956090/a537c483-abeb-43aa-bc27-c3aa90ae00a0"></Videos>
       <Videos title ="Gatito" parafo="Gatito...." video="https://github.com/lizmariela/imagen/assets/147956090/ff859589-df4c-4b16-9896-2d2667d950f4"></Videos>
       <Videos title ="Amigos" parafo="no autor" video="https://github.com/lizmariela/imagen/assets/147956090/e329f915-edf4-4a3b-ad15-a3ec45e4a0ee"></Videos>
      </div>
    
    </main>
  );
}
