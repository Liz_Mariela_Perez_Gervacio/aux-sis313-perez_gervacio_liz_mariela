export default function Button(props:{title:string,transparente?:boolean}){
    return (
        props.transparente?
        <button className="color1 color3">{props.title}</button>
        :
        <button className="color1 color2">{props.title}</button>
        
    )
}
